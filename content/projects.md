+++
title = "Projects"
path = "projects"
+++

### SlackOS | [GitLab](https://gitlab.com/Ripudaman_Singh/slackos)

- A 64-bit Kernel plus bootloader for the x86 architecture using C and assembly.
- Implemented keyboard and video drivers.
- Bootloader and ISR are written in Assembly, rest is written in C.

### TinyCPU | [GitLab](https://gitlab.com/Ripudaman_Singh/tinycpu)

- Built a simple 16 Bit computer which is inspired by MIPS and intel 8086 architecture.
- A custom assembler written in Zig programming language for convenience.
- Digital (v0.27) used to simulate circuit, Zig used to write assembler.

### Clrdoc | [GitLab](https://gitlab.com/twinkletoes123/clrdoc)

- Developed a simple deep-learning based solution for noise reduction in text-images. It attempts to remove stains, wrinkles, noise/dirt from the image using an autoencoder neural network.
- Used technologies such as Saturn Cloud, Tch-rs, Torchscript and Rust programming language.

### attoC | [GitLab](https://gitlab.com/Ripudaman_Singh/minic)

- Developed a small compiler, consists of lexer, parser, syntax tree generator, Intermediate code generator and a virtual
machine.
- Build using Rust programming language.

### shdlr | [GitLab](https://gitlab.com/twinkletoes123/shdlr)

- Developed a time table generator using constraint satisfaction algorithm to generate suitable time table.
- Incorporates various constraints such as teacher availability, date and time, available rooms etc.
- Built using Rust programming language in the backend and for the front-end used eFrame, an immediate mode library.
